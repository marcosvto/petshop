import { QueryContract } from '../contracts/query/query.contract';
import { QueryDto } from '../dtos/query.dto';
import { CreatePetContract } from '../contracts/customer/create-pet.contract';
import { Pet } from '../models/pet.model';
import { BadRequestException, Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Post, Put, Res, UseInterceptors } from "@nestjs/common";

// Interceptors
import { ValidatorInterceptor } from "src/interceptors/validator.interceptor";

// Contracts
import { CreateCustomerContract } from "../contracts/customer/create-customer.contract";
import { CreateAddressContract } from '../contracts/customer/create-address.contract';

// Dtos
import { CreateCustomerDto } from "../dtos/create-customer-dto";

// Models
import { Customer } from "../models/customer.model";
import { Result } from "../models/result.model";
import { User } from "../models/user.model";
import { Address } from '../models/address.model';

// Services
import { AccountService } from "../services/account.service";
import { CustomerService } from "../services/customer.service";
import { model } from 'mongoose';

@Controller('v1/customers')
export class CustomerController {

    constructor(
        private readonly accountService: AccountService,
        private readonly customerService: CustomerService
    ) {}

    @Get()
    async getAll() {
      const customers = await this.customerService.findAll();
      return new Result(null, true, customers, null);
    }

    @Get(':document')
    async get(@Param('document') document: string) {
      const customer = await this.customerService.find(document);
      return new Result(null, true, customer, null);
    }

    @Post()
    @UseInterceptors(
        new ValidatorInterceptor(new CreateCustomerContract())
    )
    async post(@Body() body: CreateCustomerDto) {
        try {
            const user: User = await this.accountService.create(
                new User(body.document, body.password, true)
            );
            
            const customer = new Customer(
                body.name,
                body.document,
                body.email,
                null,
                [],
                null,
                null,
                null,
                user
            );

            const res = await this.customerService.create(customer);

            return new Result('ok', true, body, null);
            
        } catch (error) {
            throw new HttpException(new Result('Não foi possível adicionar customer', false, null, error), HttpStatus.BAD_REQUEST)
        }
     
    }

    @Post(':document/addresses/billing')
    @UseInterceptors(new ValidatorInterceptor(new CreateAddressContract()))
    async addBillingAddress(@Body() model: Address, @Param('document') document: string) {
      try {
        await this.customerService.addBillingAddress(document, model);
        return new Result(null, true, model, null);
      } catch (error) {
        throw new HttpException(new Result('Não foi possível adicionar seu endereço', false, null, error), HttpStatus.BAD_REQUEST);
      }
    }

    @Post(':document/addresses/shipping')
    @UseInterceptors(new ValidatorInterceptor(new CreateAddressContract()))
    async addShppingAddress(@Body() model: Address, @Param('document') document: string) {
      try {
        await this.customerService.addBillingAddress(document, model);
        return new Result(null, false, model, null);
      } catch (error) {
        throw new HttpException(new Result('Não foi possível adicionar seu endereço', false, null, error), HttpStatus.BAD_REQUEST);
      }
    }

    @Post(':document/pets')
    @UseInterceptors(new ValidatorInterceptor(new CreatePetContract()))
    async createPet(@Body() model: Pet, @Param('document') document: string) {
      try {
        await this.customerService.createPet(document, model);
        return new Result(null, true, model, null);
      } catch (error) {
        throw new HttpException(new Result('Não foi possível adicionar seu pet', false, null, error), HttpStatus.BAD_REQUEST);
      }
    }

    @Put(':document/pets/:id')
    @UseInterceptors(new ValidatorInterceptor(new CreatePetContract()))
    async updatePet(@Param('document') document: string, @Param('id') id, @Body() model: Pet) {
      try {
        await this.customerService.updatePet(document, id, model);
        return new Result(null, true, model, null);
      } catch (error) {
        throw new HttpException(new Result('Não foi possível atualizar seu pet', false, null, error), HttpStatus.BAD_REQUEST);
      }
    }

    @Delete(':document/pets/:id')
    async deletePet(@Param('document') document: string, @Param('id') id) {
      try {
        await this.customerService.removePet(document, id);
        return new Result('ok', true, null, null);
      } catch (error) {
        throw new HttpException(new Result('Não foi possível remover seu pet', false, null, error), HttpStatus.BAD_REQUEST)
      }
    }

    @Post('query')
    @UseInterceptors(new ValidatorInterceptor(new QueryContract()))
    async query(@Body() model: QueryDto) {
      const custormes = await this.customerService.query(model);
      return new Result(null, true, custormes, null);

    }
}