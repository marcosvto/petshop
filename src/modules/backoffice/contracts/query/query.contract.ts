import { Contract } from 'src/modules/backoffice/contracts/contract';
import { Flunt } from '../../../../utils/flunt';
import { QueryDto } from '../../dtos/query.dto';

export class QueryContract implements Contract {
  errors: any[];
  validate(model: QueryDto): boolean {
    const flunt = new Flunt();

    flunt.lessThen(model.take, 25, 'O valor do take tem ser menor que 25!');
    flunt.biggerThen(model.skip, 0, 'O valor o skip tem que ser maior ou igual 0!');
    flunt.lessThen(model.skip, 25, 'O valor do skip tem ser menor que 25!!');

    if (model.skip > model.take) {
      flunt.errors.push('O valor do skip não poder ser maior que o take');
    }

    this.errors = flunt.errors;
    
    return flunt.isValid();
  }
  
}