import { Flunt } from '../../../../utils/flunt';
import { Contract } from 'src/modules/backoffice/contracts/contract';
// Models
import { Pet } from '../../models/pet.model';

export class CreatePetContract implements Contract {
  errors: any[];
  validate(model: Pet): boolean {
    const flunt = new Flunt();

    flunt.hasMinLen(model.name, 2, 'Nome Inválido');
    flunt.hasMinLen(model.gender, 3, 'Gênero Inválido');
    flunt.hasMaxLen(model.kind, 3, 'Tipo inválido');
    flunt.hasMinLen(model.brand, 3 , 'Raça inválida');

    this.errors = flunt.errors;

    return flunt.isValid();
  }

}