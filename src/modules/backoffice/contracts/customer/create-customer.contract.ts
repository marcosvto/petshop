import { Injectable } from "@nestjs/common";
import { Flunt } from "src/utils/flunt";
import { CreateCustomerDto } from "../../dtos/create-customer-dto";
import { Customer } from "../../models/customer.model";
import { Contract } from "../contract";

@Injectable()
export class CreateCustomerContract implements Contract {
    errors: any[];

    validate(dto: CreateCustomerDto): boolean {

        const flunt = new Flunt();

        console.log(dto);

        if (Object.keys(dto).length === 0) {
            this.errors = ['No payload found'];
            return false;
        }

        flunt.isRequired(dto.name, 'Campo é obrigatório');
        flunt.hasMinLen(dto.name, 5, 'Nome inválido');

        flunt.isEmail(dto.email, 'E-mail inválido');
        flunt.isFixedLen(dto.document, 11 ,'CPF Inválido');

        flunt.isRequired(dto.password, 'Password é Obrigatório');
        flunt.hasMinLen(dto.password, 8, 'Password inválido');

        this.errors = flunt.errors;

        return this.errors.length === 0;
    }
}