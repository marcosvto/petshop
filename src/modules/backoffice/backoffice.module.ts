import { Module } from '@nestjs/common';

// Modules
import { MongooseModule } from '@nestjs/mongoose';

// Controllers
import { CustomerController } from './controllers/customer.controller';

// Schemas
import { CustomerSchema } from './schemas/customer.schema';
import { UserSchema } from './schemas/user.schema';

// Services
import { AccountService } from './services/account.service';
import { CustomerService } from './services/customer.service';

@Module({
    imports: [
        MongooseModule.forFeature([
            {
                name: 'Customer',
                schema: CustomerSchema,
            },
            {
                name: 'User',
                schema: UserSchema
            }
        ])
    ],
    controllers: [
        CustomerController
    ],
    providers: [
        AccountService,
        CustomerService
    ]
})
export class BackofficeModule {}
