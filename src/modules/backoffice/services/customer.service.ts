import { QueryDto } from '../dtos/query.dto';
import { Pet } from '../models/pet.model';
import { CustomerDocument } from '../schemas/customer.schema';
import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model, Document, Query } from 'mongoose';

// Models
import { Address } from "../models/address.model";
import { Customer } from "../models/customer.model";

@Injectable()
export class CustomerService {
  constructor(
    @InjectModel('Customer') private readonly model: Model<CustomerDocument>
  ) { }

  async create(customer: Customer) {
    const model = new this.model(customer);
    return await model.save();
  }

  // Adiciona um novo endereço de cobrança ao cliente
  async addBillingAddress(document: string, data: Address): Promise<Customer> {
    const options = { upsert: true };
    return await this.model.findOneAndUpdate({ document }, {
      $set: {
        billingAddress: data,
      }
    }, options)
  }

  async createPet(document: string, data: Pet) {
    const options = { upsert: true, new: true };
    return await this.model.findOneAndUpdate({ document }, {
      $push: {
        pets: data
      }
    }, options);
  }

  async updatePet(document: string, id: string, pet: Pet) {
    return await this.model.findOneAndUpdate({ document, 'pets._id': id }, {
      $set: {
        'pets.$': pet
      }
    });
  }

  async removePet(document: string, id: string) {
    return await this.model.updateOne({ document }, {
      $pull: {
        'pets': {
          _id: id
        }
      },
    });
  }

  async findAll(): Promise<Customer[]> {
    return await this.model.find({}, 'name email document pets')
      .sort('name')
      .exec();
  }

  async find(document: string): Promise<Customer> {
    return await this.model
      .findOne({ document })
      .populate('user', 'username')
      .exec();
  }

  async query(model: QueryDto): Promise<Customer[]> {
    return await this.model.find(model.query, model.fields, 
      { skip: model.skip, limit: model.take})
      .sort(model.sort)
      .exec()
  }

}