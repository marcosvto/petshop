import { isNumber } from "node:util";

export class Flunt {
    constructor(public errors: any[] = []) {}

    isRequired(value, message) {
        if (!value || value.length <= 0) {
            this.errors.push(message);
        }
    }

    hasMinLen = (value, min, message) => {
        if (!value || value.length < min) {
            this.errors.push(message);
        }
    }

    hasMaxLen = (value, max, message) => {
        if (!value || value.length > max) {
            this.errors.push(message);
        }
    }

    isFixedLen = (value, len, message) => {
        if (!value || value.length !== len) {
            this.errors.push(message);
        }
    }

    isEmail = (value, message) => {
        const reg = new RegExp(/\S+@\S+\.\S+/);
        if (!reg.test(value)) {
            this.errors.push(message);
        }
    }

    lessThen(value, len, message) {
      if (typeof value != 'number' || value > len) {
        this.errors.push(message);
      }
    }

    biggerThen(value, len, message) {
      if (typeof value != 'number' || value < len) {
        this.errors.push(message);
      }
    }

    clear() {
        this.errors = [];
    }

    isValid() {
        return this.errors.length === 0;
    }
}