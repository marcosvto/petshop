import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { BackofficeModule } from 'src/modules/backoffice/backoffice.module';

// url antiga (mongodb://dev:123456@localhost:27017/admin)
@Module({
  imports: [
    MongooseModule.forRoot('mongodb+srv://japaodev_root:46557985@cluster0.s2dso.mongodb.net/petshop?retryWrites=true&w=majority', {
      useNewUrlParser: true,
      useFindAndModify: true,
    }),
    BackofficeModule]
  ,
  controllers: []
})
export class AppModule {}
